/** 功能：初始化bbs-util工具类,最后引入,
 * 引入顺序：touchjs.core.js -> touchjs.ajax.js -> touchjs.dialog.js -> touchjs.form.js -> touchjs.modules.js -> 其他自定义扩展模块 -> touchjs-init.js
 * 创建人：吕凯 2020-11-27*/
(function() {
	var contentE = $("body");
	Touchjs.touch(contentE); //与Touchjs.load方法类似，两者区别是touch会对表单进行处理为ajax提交，load不会
})();