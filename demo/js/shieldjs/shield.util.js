/*工具*/
;(function($){
	/**
	 * 序列表单内容为json对象
	 * @constructor $().serializeJson
	 * @author kevin Lv
	 * @returns {Json} json对象
	 * @example $("form").serializeJson() 
	 * 
	 */
	$.fn.serializeJson = function(){
		var serializeObj = {};  
		var array = this.serializeArray(); //获取不到未选中的CheckBox的name和value值
		//var str = this.serialize();
		//console.log("array")
		//console.log(array)
		$(array).each(function(){
			if ( serializeObj[this.name] || serializeObj[this.name]==="" ) { //空字符串会处理为null
				if($.isArray(serializeObj[this.name])){
					serializeObj[this.name].push(this.value);
				} else {
					serializeObj[this.name] = [serializeObj[this.name],this.value];
				}
			} else {
				serializeObj[this.name] = this.value;
			}
		});
		return serializeObj;
	};
	/**
	 * 动态监听内容变化
	 * @constructor $().contentChange
	 * @author kevin Lv
	 * @param selector {String} jQuery选择器
	 * @param callbackFn {Function} 回调函数
	 * @example $("body").contentChange("body", function(){}); 
	 * 
	 */
	$.fn.contentChange= function(selector, callbackFn){
		/**
		* @description 处理函数
		*/
		this.each(function(){
			var containerE = $(this); 
			containerE.on('DOMNodeInserted', selector, function(){ //ie8以上支持
				callbackFn($(this));
			});
		});
	};
})(jQuery);
/**
 * resize增强版（默认只有浏览器窗口调整大小，修改后可监听div或其它元素）
 * @constructor $().resize
 * @author 来自网络
 * @param callbackFn {Function} 回调函数
 * @example $("#div1").resize(function(){}); 
 * 
 */
;(function($, h, c) {
	var a = $([]), e = $.resize = $.extend($.resize, {}), i, k = "setTimeout", j = "resize", d = j
			+ "-special-event", b = "delay", f = "throttleWindow";
	e[b] = 350;
	e[f] = true;
	$.event.special[j] = {
		setup : function() {
			if (!e[f] && this[k]) {
				return false
			}
			var l = $(this);
			a = a.add(l);
			$.data(this, d, {
				w : l.width(),
				h : l.height()
			});
			if (a.length === 1) {
				g()
			}
		},
		teardown : function() {
			if (!e[f] && this[k]) {
				return false
			}
			var l = $(this);
			a = a.not(l);
			l.removeData(d);
			if (!a.length) {
				clearTimeout(i)
			}
		},
		add : function(l) {
			if (!e[f] && this[k]) {
				return false
			}
			var n;
			function m(s, o, p) {
				var q = $(this), r = $.data(this, d);
				r.w = o !== c ? o : q.width();
				r.h = p !== c ? p : q.height();
				n.apply(this, arguments)
			}
			if ($.isFunction(l)) {
				n = l;
				return m
			} else {
				n = l.handler;
				l.handler = m
			}
		}
	};
	function g() {
		i = h[k](function() {
			a.each(function() {
				var n = $(this), m = n.width(), l = n.height(), o = $
						.data(this, d);
				if (m !== o.w || l !== o.h) {
					n.trigger(j, [ o.w = m, o.h = l ])
				}
			});
			g()
		}, e[b])
	}
})(jQuery, this);
/**
 * 将值插入到光标位置
 * @constructor $().insertAtCursor
 * @author 来自网络
 * @param myValue {String} 赋值
 * @example $("#div1").insertAtCursor('123');
 * 
 */
$.fn.extend({
    insertAtCursor : function(myValue) {
    	var $t = $(this)[0];
		if (document.selection) {
			this.focus();
			sel = document.selection.createRange();
			sel.text = myValue;
			this.focus();
		} else if ($t.selectionStart || $t.selectionStart == '0') {
			var startPos = $t.selectionStart;
			var endPos = $t.selectionEnd;
			var scrollTop = $t.scrollTop;
			$t.value = $t.value.substring(0, startPos) + myValue + $t.value.substring(endPos, $t.value.length);
			this.focus();
			$t.selectionStart = startPos + myValue.length;
			$t.selectionEnd = startPos + myValue.length;
			$t.scrollTop = scrollTop;
		} else {
			this.value += myValue;
			this.focus();
		}
	}
});
/**
 * 获取选中元素对象（对象中包含开始位置、结束位置、内容）
 * @constructor $().selection
 * @author 来自网络
 * @example $("#div1").selection().text; 
 * 
 */
$.fn.selection = function(){
	var s,e,range,stored_range;
	if (this[0].selectionStart == undefined) {
		var selection=document.selection;
		if (this[0].tagName.toLowerCase() != "textarea") {
			var val = this.val();
			range = selection.createRange().duplicate();
			range.moveEnd("character", val.length);
			s = (range.text == "" ? val.length:val.lastIndexOf(range.text));
			range = selection.createRange().duplicate();
			range.moveStart("character", -val.length);
			e = range.text.length;
		} else {
			range = selection.createRange(),
			stored_range = range.duplicate();
			stored_range.moveToElementText(this[0]);
			stored_range.setEndPoint('EndToEnd', range);
			s = stored_range.text.length - range.text.length;
			e = s + range.text.length;
		}
	} else {
		s=this[0].selectionStart,
		e=this[0].selectionEnd;
	}
	var te=this[0].value.substring(s,e);
	return {start:s, end:e, text:te};
};
/**
 * ShieldMap对象
 * @constructor ShieldMap
 * @author kevin Lv
 * @example  示例：
 * var myMap = new ShieldMap();
 * myMap.put("key","value");
 * var key = myMap.get("key");
 * myMap.remove("key");
 * 
 */
function ShieldMap(){

	this.elements = new Array();
	
	this.size = function(){
		return this.elements.length;
	}
	
	this.isEmpty = function(){
		return (this.elements.length < 1);
	}
	
	this.clear = function(){
		this.elements = new Array();
	}
	
	this.put = function(_key, _value){
		this.remove(_key);
		this.elements.push({key: _key, value: _value});
	}
	
	this.remove = function(_key){
		try {
			for (i = 0; i < this.elements.length; i++) {
				if (this.elements[i].key == _key) {
					this.elements.splice(i, 1);
					return true;
				}
			}
		} catch (e) {
			return false;
		}
		return false;
	}
	
	this.get = function(_key){
		try {
			for (i = 0; i < this.elements.length; i++) {
				if (this.elements[i].key == _key) { return this.elements[i].value; }
			}
		} catch (e) {
			return null;
		}
	}
	
	this.element = function(_index){
		if (_index < 0 || _index >= this.elements.length) { return null; }
		return this.elements[_index];
	}
	
	this.containsKey = function(_key){
		try {
			for (i = 0; i < this.elements.length; i++) {
				if (this.elements[i].key == _key) {
					return true;
				}
			}
		} catch (e) {
			return false;
		}
		return false;
	}
	
	this.values = function(){
		var arr = new Array();
		for (i = 0; i < this.elements.length; i++) {
			arr.push(this.elements[i].value);
		}
		return arr;
	}
	
	this.keys = function(){
		var arr = new Array();
		for (i = 0; i < this.elements.length; i++) {
			arr.push(this.elements[i].key);
		}
		return arr;
	}
}
/**
 * Dialog对象
 * @constructor Dialog
 * @author kevin Lv
 * @example  示例：
 * var dialog = new Dialog();
 * dialog.put(ele, index, width, height, preEle, preEleIndex);
 * 
 */
function Dialog(){
	/**	弹出框元素对象
		@type jQueryObject
	 */
	this.ele = {};
	/**	下标
		@type int
	 */
	this.index = -1;
	/**	宽度
		@type int
	 */
	this.width = 0;
	/**	高度
		@type int
	 */
	this.height = 0;
	/**	上一个弹出框元素下标
		@type jQueryObject
	 */
	this.preEleIndex = null;
	/**	上一个弹出框元素
		@type jQueryObject
	 */
	this.preEle = null;
	/**	标题
		@type String
	 */
	this.title = "";

	this.init = function (ele, index, width, height, preEle, preEleIndex){
		ele.data("index", index);
		this.ele = ele;
		if (index) {
			this.index = index;
		}
		if (width) {
			this.width = width;
        } else {
        	this.width = ele.width();
		}
		if (height) {
			this.height = height;
		} else {
        	this.height = ele.height();
		}
		if (preEleIndex) {
			this.preEleIndex = preEleIndex;
        }
		this.preEle = preEle;
	}
}
/**
 * 根据传入字符串返回jQuery对象，如果本身为jquery对象直接返回
 * @param selector {Object} jQuery对象或者jQuery选择器
 * @returns {jQueryObject} jQuery对象
 */
function getjQueryObj(selector){
	if (selector) {
		if(!(selector instanceof jQuery)){ 
			selector = $(selector);
		}
    }
	return jQObjEmpty2Null(selector);
}
/**
 * 是否存在属性
 * @param jobj {jQueryObject} 判断的对象
 * @param attrname {String} 属性名
 * @returns {Boolean} 存在该属性则返回true，否则返回false
 */
function hasAttr(jobj, attrname){
	if(!jobj){
		return false;
	}
	return typeof(jobj.attr(attrname))!="undefined";
}
/**
 * 获取一个jQuery对象的扩展设置（取属性shieldSettings的设置值覆盖，不存在则取jQuery.data()设置的属性，）
 * @param ele {jQueryObject} 待处理的对象
 * @param _default {Json} 默认设置
 * @returns {Json} 参数设置
 */
function getExtendSettings(ele, _default){
	_default = _default||{};
	var settings = getParams(ele, {}, "shieldSettings");
	var datas = $.extend(ele.data(), {}); //克隆出来防止影响原属性
	delete datas["shieldParams"];//删除属性
	delete datas["shieldSettings"];//删除属性
	for(var key in datas){
		if (key.match("^_")) { //下划线开头的删除
			delete datas[key];//删除属性
		}
	}
	return $.extend(true, _default, datas, settings);
};
/**
 * 获取一个jQuery对象的设置（取属性shieldSettings的设置值）
 * @param ele {jQueryObject} 待处理的对象
 * @param _default {Json} 默认设置
 * @returns {Json} 参数设置
 */
function getSettings(ele, _default){
	var settings = getParams(ele, _default||{}, "shieldSettings");
	return settings;
};
/**
 * 获取参数
 * @param ele {jQueryObject} 待处理的对象
 * @param params {Json} 默认设置
 * @param attrName {String} 属性名
 * @returns {Json} 参数设置
 */
function getParams(ele, params, attrName){
	if (ele.length>0) {
		try {
			if (!attrName) {
				attrName = "shieldParams";
			}
			if (hasAttr(ele, attrName)) { //有参数时从shieldParams取
				params = $.metadata ? $.extend({}, params, ele.metadata({ 
					"type": "attr","name": attrName, "single":attrName
				})) : params;
			} else { //无参数时默认从class取
				params = $.metadata ? $.extend({}, params, ele.metadata()) : params;
			}
			return params;
		} catch (e) {
			var name = ele.attr("name");
			if (!name) {
				name = ele.val();
			}
			alert("获取参数异常："+name+" "+e);
			return false;
		}
	}
	return null;
};
/**
 * 获取时间参数，转换为毫秒
 * @param  ele {jQueryObject} 待处理的对象
 * @param  attrName {String} 属性名
 * @param  defaultValue {Number} 默认值
 * @returns {Number} 时间值
 */
function getTimeAttr(ele, attrName, defaultValue){
	var time = defaultValue;
	if (hasAttr(ele, attrName)) {
		var timeSrc = ele.attr(attrName).toLowerCase();
		var time = parseFloat(timeSrc);
		time = time*1000; //默认是秒
		if (timeSrc.indexOf("m") != -1 || timeSrc.indexOf("min") != -1) {
			time = time*60; //分钟，1min=60s
		}
		if (timeSrc.indexOf("h") != -1 || timeSrc.indexOf("hour") != -1) {
			time = time*3600; //小时，1h=3600s
		}
    }
	return time;
};
/**
 * 全部替换，默认替换只替换第1个
 * @constructor String.replaceAll
 * @param s1 {String} 替换前字符串
 * @param s2 {String} 替换后字符串
 * @returns {String} 
 */
String.prototype.replaceAll = function(s1, s2){
	//gm g=global, m=multiLine
	return this.replace(new RegExp(s1,"gm"),s2);
}
/**
 * 获取并标记获取到的元素，防止重复添加
 * @param  ele {jQueryObject} 待处理的父对象
 * @param selector {String} 选择器
 * @see {@link findEle} 注意区别
 * @returns {jQueryObject}
 */
function findAndMarkEle(ele, selector) {
	var markSelector = selector.replaceAll(" ","").replaceAll("\\.","_").replaceAll("#","_sdc01_").replaceAll("\\:","_sdc02_").replaceAll(">","_sdc03_")
	.replaceAll("\\+","_sdc04_").replaceAll("\\~","_sdc05_").replaceAll("=","_sdc06_").replaceAll("\\!","_sdc07_").replaceAll("\\^","_sdc08_")
	.replaceAll("\\$","_sdc09_").replaceAll("\\*","_sdc10_").replaceAll("\\[","_sdc11_").replaceAll("\\]","_sdc12_").replaceAll("\\(","_sdc13_")
	.replaceAll("\\)","_sdc14_").replaceAll("'","_sdc15_").replaceAll("\"","_sdc16_").replaceAll(",","_sdc17_");
//	var ele = ele.find(selector + ":not(.shieldMarked"+markSelector+",.noshield)"); //此写法在selector为多重选择器时有问题，如弹出框.shieldDialog,.shieldLookup
	var ele = ele.find(selector).not(".shieldMarked"+markSelector+",.noshield");
	ele.addClass("shieldMarked"+markSelector);
	ele.data("shieldMarked"+markSelector, "marked");
	return ele;
};
//
/**
 * 获取但不标记取到的元素
 * @param ele {jQueryObject} 待处理的父对象
 * @param selector {String} 选择器
 * @see {@link findAndMarkEle} 注意区别
 * @see {@link findAndMarkEleOn} 注意区别
 * @returns {jQueryObject}
 */
function findEle(ele, selector) {
	var ele = ele.find(selector + ":not(.noshield)");
	return ele;
};
/**
 * 获取并标记元素，动态绑定事件(待完善)
 * @param ele {jQueryObject} 待处理的父对象
 * @param selector {String} 一个选择器字符串用于过滤器的触发事件的选择器元素的后代。如果选择的为null或省略，当它到达选定的元素，事件总是触发。
 * @param events {String} 一个或多个用空格分隔的事件类型和可选的命名空间，如"click"或"keydown.myPlugin" 。
 * @see {@link findAndMarkEle} 注意区别
 * @returns {jQueryObject}
 */
function findAndMarkEleOn(ele, selector, events) {
	var ele = findAndMarkEle(ele, selector);
	
	ele.off(events, selector).on(events, selector, function(event){
		var shieldForwardEle = $(this);
		if (hasAttr(shieldForwardEle,"toShieldMenu")) { //有菜单属性才处理
			var toShieldMenuArr = shieldForwardEle.attr("toShieldMenu").split(">");
			if (toShieldMenuArr.length > 1) {
				var toNavMenu = $(toShieldMenuArr[0]);
				shieldForwardEle.attr("showclass",toShieldMenuArr[1]); //添加属性，跳转完成后移除
				toNavMenu.click();
			}
		}
	});
	
	return ele;
};
/**
 * 格式化url，处理为标准格式的url
 * @param url {String} 待处理的url
 * @returns {String}
 */
var formatUrl = function(url) {
    if (!url || url == '/') {
        return '';
    }
    if (url.indexOf('http://') == 0 || url.indexOf('https://') == 0) {
        return url;
    }
    if(url.indexOf('/') == 0){ //本地路径
    	return url;
    }else{
    	return 'http://' + url;
    }
};
/**
 * 打开新页面
 * @param ele {jQueryObject} 待处理的对象
 * @param href {String} 超链接
 * @param params {Json} 参数
 */
var openNewPage = function(ele, href, params) {
	var formEs = ele.closest("form");
	if (formEs.length > 0) {
		ele = formEs; //包含在form内时需要插入到form外部，否则提交的是父form
    }
	var method = ele.data("method")||"POST";
	var html = "<form action='" + href + "' target='_blank' method='"+method+"'>";
	for(var key in params){
		if($.isArray(params[key])){
			for(var i=0;i<params[key].length;i++) {
				html += "<input type='hidden' name='" + key + "' value='" + params[key][i] + "'>"; //直接取是逗号连接的字符串
            }
		} else {
			html += "<input type='hidden' name='" + key + "' value='" + params[key] + "'>";
		}
	}
	html+= "</form>";
	ele.after(html);
	var formE = ele.next();
	formE.submit(function(){
		return true;
	}).submit(); //直接用a.click()不执行
	formE.remove();
}
/**
 * 替换指定传入参数的值, paramName为参数, replaceWith为新值
 * @param oUrl {String} 待处理的url
 * @param paramName {String} 处理参数
 * @param replaceWith {String} 参数处理后的值
 * @returns {String}
 */
function replaceParamVal(oUrl, paramName, replaceWith) {
    var re = eval('/('+ paramName+'=)([^&]*)/gi');
    var nUrl = oUrl.replace(re, paramName+'='+replaceWith);
    return nUrl;
}
/**
 * 日期格式化（待删除，改为moment.js）
 * @param date {Date} 带格式话的日期
 * @param format {String} 格式化的表达式，如：yyyy-MM-dd
 * @returns {String}
 */
var dateFormat = function(date, format) {
	if (format === undefined) {
		format = date;
		date = new Date();
	}
	var map = {
		"M" : date.getMonth() + 1, // 月份
		"d" : date.getDate(), // 日
		"H" : date.getHours(), // 小时
		"h" : date.getHours(), // 小时
		"m" : date.getMinutes(), // 分
		"s" : date.getSeconds(), // 秒
		"q" : Math.floor((date.getMonth() + 3) / 3), // 季度
		"S" : date.getMilliseconds()
	// 毫秒
	};
	format = format.replace(/([yMdHhmsqS])+/g, function(all, t) {
		var v = map[t];
		if (v !== undefined) {
			if (all.length > 1) {
				v = '0' + v;
				v = v.substr(v.length - 2);
			}
			return v;
		} else if (t === 'y') {
			return (date.getFullYear() + '').substr(4 - all.length);
		}
		return all;
	});
	return format;
};
/**
 * 参数名适配,返回json格式，键为源字段名值为目标字段名
 * @param ele {jQueryObject} 待处理的对象
 * @returns {Map}
 */
var paramNameAdapt = function(ele) {
	var paramAdapter = ele.attr("paramAdapter");
	var params = {};
	if (paramAdapter) { //有值且不为空
		//paramAdapter="id>typeId#newsType"
		var paramAdapters = paramAdapter.split("#"); //#号分隔
		for(var i=0;i<paramAdapters.length;i++){
			var paramName = paramAdapters[i];
			if (paramName.indexOf(">")!=-1) {
				var paramNames = paramName.split(">"); //如果key与提交不一致用>号分隔
				var sourceName = paramNames[0];
				var targetName = paramNames[1];
				params[sourceName] = targetName;
			} else {
				params[paramName] = paramName;
			}
		}
	}
	return params;
}
/**
 * 判断jquery空对象转换为null，其他的原样返回
 * @param ele {jQueryObject} 待处理的对象
 * @returns {jQueryObject}
 */
var jQObjEmpty2Null = function(ele) {
	if (!ele || ele.length == 0) {
	    return null;
    }
	return ele;
}
/**
 * 移除字符串中的html字符
 * @param str {String} 待处理的字符
 * @returns {String}
 */
var removeHtml = function(str) {
	return jQuery("<p>"+str+"</p>").text();
}
/**
 * 随机取色
 * @returns {String} 16进制表示的颜色字符串
 */
var getRandomColor = function(){
	return  '#' + (function(color){
    	return (color +=  '0123456789abcdef'[Math.floor(Math.random()*16)])
      	&& (color.length == 6) ?  color : arguments.callee(color);
  	})('');
}
/**
 * 打印堆栈信息
 * @returns {String}
 */
function printCallStack(error) {
	var stackStr = "";
	if (console && console.trace){ //ie8下不支持trace
		console.trace(error);
	}
	return stackStr;
}
/**
 * 返回当前js的路径
 * @returns {String}
 */
function getJSPath(){
	var currentScript=document.currentScript?document.currentScript.src:function(){
		var jssrc;
		var jss=document.scripts;
		for(var i=jss.length-1,n=i;n>0;n--){
			if("interactive"===jss[n].readyState){ //已经加载
				jssrc=jss[n].src;
				break;
			}
			return jssrc||jss[i].src
		}
	}();
	return currentScript;
}
/**
 * 深层拷贝json对象
 * @param jsonObj {JSON} 待处理的对象
 * @returns {JSON}
 */
function copyJson(jsonObj){
	// var newObj = JSON.parse(JSON.stringify(jsonObj)); //深层拷贝
	var newObj = $.extend(true, {}, jsonObj);
	return newObj;
}
/**
 * 输出json字符串
 * @param jsonObj {JSON} 待处理的对象
 * @returns {String}
 */
function json2String(jsonObj){
	return JSON.stringify(jsonObj);
}
/**
 * 返回当前js的目录
 * @returns {String}
 */
function getJSDir(){
	var jsPath = getJSPath();
	return jsPath.substring(0,jsPath.lastIndexOf("/")+1)
}
//ie8不支持isArray的方法
if (!Array.isArray) {
	Array.isArray = function(arg) {
		return Object.prototype.toString.call(arg) === '[object Array]';
	};
}

//根据参数选择器获取参数值，用于动态元素传参
;(function($){
	/**
	 * 根据参数选择器获取参数值，用于动态元素传参
	 * @constructor $().serializeJson
	 * @author kevin Lv
	 * @returns {Json} json对象
	 * @example $("form").serializeJson() 
	 * 
	 */
	$.fn.paramSelector = function(options){
		if (options == null) {
			options = {};
        }
        options = $.extend(true, {}, $.fn.paramSelector.defaultOptions, options);
        
		var eles = $(this); //表单
		var result = {};
		eles.each(function(){
			var $this = $(this);
			
			var optionsEle = getExtendSettings($this, options); //data-xx可能会覆盖取到的值
			var mainEle = options.mainEle ? getjQueryObj(options.mainEle) : null; //主内容区
        	var params = getParamSelector($this, {}, mainEle, optionsEle); 
        	if(!params){
        		result = false;
        		return false; //true=continue，false=break。 
        	}
        	
        	result = $.extend(true, result, params);
		});
		return result;
	}
	
	/**
	 * 获取参数选择器的参数，如果正常返回json格式的参数否则返回false
	 * @param ele {jQueryObject} 待处理的对象
	 * @param params {Json} 默认设置
	 * @param mainEle {jQueryObject} 关联查找对象
	 * @returns {Json} 参数设置
	 */
	var getParamSelector = function(ele, params, mainEle, options){
		if (!mainEle || mainEle.length == 0) {
			mainEle = $(options.mainEle); //主内容区
	    }
		if (hasAttr(ele, options.attrName)) { //参数选择器
			var nodatatips = ele.data("noneTips"); //无数据的提示

			var paramSelector = ele.attr(options.attrName);
			/*{selectRows}表示复选框选中行的数据作为参数提交，如果只想选一部分可以在里面增加选择器
			 {selectRows>:checkbox}意思为只提交选中行的的checkbox作为参数,用'>'连接选择器，里面的选择器为jQuery选择器*/
			if (paramSelector.indexOf("{selectRows") != -1 ) { //选中行用>分隔内部选择器
				// 括号表示组。访问可以用group[index]来访问每组的信息  
				var linkRegx = /.*{(selectRows.*?)},?/;  
				var group = paramSelector.match(linkRegx);
				if (group.length == 2) { //如果包含则为长度为2
					var selectRows = group[1]; //selectRows
					var selectRowsEle = mainEle.find("."+options.checkedClass);
					if(selectRows.indexOf(">") > 0){ //过滤
						var selectRowsArrs = selectRows.split(">");
						selectRowsEle = selectRowsEle.find(selectRowsArrs[1]);
					}
					if (!selectRowsEle.is(":input")) { //如果不是input，继续查找内部的input
						selectRowsEle = selectRowsEle.find(":input");
					}
					if (options.form.validate(selectRowsEle)) {
						params = $.extend(true, {}, params, selectRowsEle.serializeJson());
						if ($.isEmptyObject(params)) {
							options.alert("消息提示", "至少选中一条信息！", "error");
							return false;
						}
	                } else {
	                	options.debug("参数选择器验证未通过！");
	                	return false;
					}
				} else {
					options.error("错误", "参数选择器参数设置错误，请检查{selectRows}！", "error");
					return false;
				}
				paramSelector = paramSelector.replace(linkRegx, "");
			}
			// 用于tr中的选择器，取当前行内参数
			if (paramSelector.indexOf("{currRows") != -1 ) {//当前行用>分隔内部选择器
				// 括号表示组。访问可以用group[index]来访问每组的信息  
				var linkRegx = /.*{(currRows.*?)},?/;  
				var group = paramSelector.match(linkRegx);
				if (group.length == 2) { //如果包含则为长度为2
					var selectRows = group[1]; //selectRows
					var selectRowsEle = ele.closest("tr");
					if(selectRows.indexOf(">") > 0 ){ //过滤
						var selectRowsArrs = selectRows.split(">");
						selectRowsEle = cselectRowsEle.filter(selectRowsArrs[1]);
					}
					if (!selectRowsEle.is(":input")) { //如果不是input，继续查找内部的input
						selectRowsEle = selectRowsEle.find(":input");
					}
					if (options.validate(selectRowsEle)) {
						params = $.extend(true, {}, params, selectRowsEle.serializeJson());
	                } else {
	                	options.debug("参数选择器验证未通过！");
	                	return false;
					}
				} else {
					options.alert("错误", "参数选择器参数设置错误，请检查{currRows}！", "error");
					return false;
				}
				paramSelector = paramSelector.replace(linkRegx, "");
			} 
			if (paramSelector) { //继续检查
				var selectEles = mainEle.find(paramSelector);
				//console.log(mainEle)
				//console.log(paramSelector)
				//console.log(selectEles)
				if (options.validate(selectEles)) {
					params = $.extend(true, {}, params, selectEles.serializeJson());
					if ($.isEmptyObject(params)) {
						options.alert("消息提示", nodatatips||"数据为空或未选中，请返回检查！", "error");
						return false;
					}
				} else {
					options.debug("参数选择器验证未通过！");
					return false;
				}
	        }
		}
		//console.log("params")
		//console.log(params);
		return params;
	}
	
	//默认参数,可扩展
    $.fn.paramSelector.defaultOptions = {
		/**    提示框，可以覆盖，默认采用alert，type字符型（success、error、info、warning）
	     * @param title {String} 标题
	     * @param message {String} 提示内容
	     * @param type {String} 提示类型包括：success、error、info、warning4种  */
	    alert : function(title, message, type){ 
	        alert(message);
	    },
	    debug : function(message){ 
	        console.log(message);
	    },
	    validate : function(formE) { //扩展方法
            return true;
        },
	    mainEle :"body", //关联对象容器，可以为jQuery选择器也可以为jQuery对象
	    checkedClass :"checked", //被选中的复选框或单选框的样式,需要结合其他的赋值
	    attrName :"paramSelector" //属性名
    };
})(jQuery);
//设置元素的值
var setInputValue = function(selectE, value, callchange, callback) {
	if (!value) {
		value = selectE.data("v");
    }
	// 参数少于3个
	if(arguments.length<=2){
		callchange = true;
	}
	if(arguments.length>2 && $.isFunction(arguments[2])){ // 第三个参数为fn
		callback = arguments[2];
		callchange = true;
	} 
	if (value || value==0) {
		if (value == "clear") { //清空值
			value = "";
        }
		selectE.val(value);
		if (callchange && selectE.val()==value ) {
			setTimeout(function() {
				selectE.triggerHandler("change"); //触发change方法
            }, 1);
        }
//		selectE.change();
	}
	selectE.data("fillv", "fill");
	selectE.attr("fillv", "fill");
	if (callback && $.isFunction(callback)) { 
		callback();
    }
};
// array移除方法
var arraysRemove = function(arrays, val) {
	var index = arrays.indexOf(val);
	if (index > -1) {
		arrays.splice(index, 1);
	}
};