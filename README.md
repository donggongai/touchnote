# touchNote

## 1. 介绍
touchNote是一款基于jQuery封装的HTML文章批注、网页备注插件，帮助开发者轻松实现类似网页在线批注。支持网页文本划线高亮、添加备注信息等。

## 2. 灵感

非常感谢如下两篇文章提供的思路：

[「划线高亮」和「插入笔记」—— 不止是前端知识点](https://zhuanlan.zhihu.com/p/225773857)

[✨ 如何用 JS 实现“划词高亮”的在线笔记功能？✨🖍️](https://juejin.cn/post/6844903827745832967)

## 3. 使用
可参考demo中的代码审查页面，运行html即可。

## 4. 说明
- 该插件目前并不完善，后续会继续更新
- 如果问题请反馈issues

### 参数说明
|key|参数|含义|
| ------------- | ------------- | ------------- |
|root|默认：document.body|开始计算偏移量的根元素|
|username|默认：兔先生|用户名，实际使用时一般为动态数据从session或cookie中取得|
|userimage|默认：|用户头像|
|parSelectors|默认：|父节点选择器，只有符合选择器的才作为父节点，用于计算文本的相对偏移量，尽量使用固定不变的节点选择器减少内容变动引起的节点变化，如段落或内容的顶级容器|
|noteContainerSelector|默认：.notesshow_container_div|备注容器jquery选择器|
|noteCountSelector|默认：.notesshow_container_div .notesCount|备注数量选择器|
|noteItemClsName|默认：notesdiv|备注条目样式名称|
|noteSelectedClsName|默认：noteselected|备注条目选中样式名称|
|hltagName|默认：span|高亮（批注）包裹标签|
|hltmpClsName|默认：tmp_touch_notes|选中区域样式名称，用于标识选中区域|
|hlClsName|默认：touch_notes|高亮（批注）样式名|
|hldatas|默认：[]|默认回显的数据，格式见后文|
|highlightNoteAfter|function，默认打开弹出框并传递当前选中的选区参数|元素高亮后的处理方法，参数(ele, data, tipsClass, tipsContentClass, options)|
|mouseup|function(event, root, ele, options)| 鼠标选择后触发的方法，参数(event, root, ele, options) |
|mouseover|function(event, root, ele, options)||
|mouseOverBefore|function(event, root, ele, options)|mouseover事件开始处理事件，返回boolean,为false则不往下进行,参数(event, root, ele, options)，如果重定义了mouseover则不调用|
|mouseOverInHigh|function(targetEle, event, root, ele, options)|mouseover事件在高亮节点的处理事件,(targetEle, event, root, ele, options)，如果重定义了mouseover则不调用|
|mouseOverAfter|function(event, root, ele, options)|mouseover事件结束方法,参数(event, root, ele, options)，如果重定义了mouseover则不调用|
|highlightNoteMouseover|[]|高亮节点的鼠标划过事件，数组，第一个function为鼠标移入事件，第二个为移出事件，分别为增加/移出selected样式|
|openDialog|function，需要自己实现|打开窗口|
|tooltopsHtml|默认：定义了划线、复制、备注3个方法|选中后悬浮窗html|
|tooltopsClickLine|function，默认：空方法|选中后悬浮窗出现的划线方法|
|tooltopsClickNote|function|选中后悬浮窗出现的批注方法|
|tooltopsClickCopy|function，默认：空方法|选中后悬浮窗出现的复制方法|
|tooltopsClickOthers|function，默认：空方法|选中后悬浮窗出现的其他方法，可自行扩展|
|addNoteHtml|function(data, index, datas, noteContainerEle, root, ele, options)|添加备注区html内容，有需要时可以覆盖,索引的class为number，用户名class为username，文本class为text，备注class为remarks，创建时间class为createtime。  参数(data, index, noteContainerEle, root, options, datas)，data为数据，index为下标,datas为所有数据，noteContainerEle为容器jquery对象，root为根元素，ele为处理的对象,options为配置|
|getAddNoteEle|function(data, index, datas, noteContainerEle, root, ele, options)|添加备注区html内容，有需要时可以覆盖。  参数(data, index, noteContainerEle, root, options, datas)，data为数据，index为下标,datas为所有数据，noteContainerEle为容器jquery对象，root为根元素，ele为处理的对象,options为配置|
|fixNoteTopFn|function(noteContainerEle, root, ele, options)|计算顶备注区部偏移量的方法|
|noteOperateHtml|默认：<div class="touch-note-popmenus down"><div class="note-tool-item"><i class="touchnote-update">修改</i></div><div class="note-tool-item"><i class="touchnote-delete">删除</i></div></div>|右侧备注栏操作html, touch-note-popmenus必须有，里面的条目必须用含有note-tool-item的样式，，修改添加样式touchnote-update，删除添加样式touchnote-delete|
|addNoteEleFn|function(noteEle, noteContainerEle, root, ele, options)|添加备注区元素事件 参数(noteEle, noteContainerEle, root, options, datas)，noteEle为备注区jquery元素，noteContainerEle为容器jquery对象，root为根元素，options为配置|
|noteUpdateFn|function(noteEle, itemEle, root, options)|备注更新 参数(noteEle, itemEle, root, options)，noteEle为备注对象，itemEle为操作条目对象，root为根元素，options为配置|
|noteDeleteFn|function(noteEle, itemEle, root, options)|备注删除 参数(noteEle, itemEle, root, options)，noteEle为备注对象，itemEle为操作条目对象，root为根元素，options为配置|
|noteClickOtherFn|function(noteEle, itemEle, root, options)|备注其他方法 参数(noteEle, itemEle, root, options)，noteEle为备注对象，itemEle为操作条目对象，root为根元素，options为配置|
|addNoteEle|function(data, index, datas, noteContainerEle, root, ele, options)|添加备注区内容 参数(data, index, noteContainerEle, root, options, datas)，data为数据，index为下标,datas为所有数据，noteContainerEle为容器jquery对象，root为根元素，options为配置|
|updateNotesCount|function(count, datas, root, options)|更新备注总数 参数(count, datas, root, options)，count为总数，datas为高亮数据，root为根元素，options为配置|
|refreshNotesCount|function(count, datas, root, options)|刷新备注区总数 参数(count, datas, root, options)，count为总数，datas为高亮数据，root为根元素，options为配置|
### 插件方法
|方法|入参介绍|含义|
| ------------- | ------------- | ------------- |
|$(dom).touchNote()|入参见上面参数说明|插件初始化方法|
|$(dom).getSelection()|-|获取当前jquery批注对象的选中内容对象|

### 数据格式

hldatas为数组格式

示例如下，nid为数据主键，keyid为touchNote为其生成的主键，keycode保存的是位置信息，在提交数据是会进行封装可以注意查看控制台，remarks为添加的备注，userid为用户id（一般由项目提供），username为用户名称（一般由项目提供），createtime为创建时间。

其中选中的文本在keycode.text中可以获取，或者使用touchNote.getSelectionText()获取当前选中内容

```json
var notesdata = [
    {'nid':'1', 'keyid':"tn-1628153524469",'keycode':{"startMeta":{"parentTagName":"DIV","parentIndex":-2,"textOffset":41},"endMeta":{"parentTagName":"DIV","parentIndex":-2,"textOffset":56},"text":"指的是句子中所有词可能构成的图","id":"tn-1628153524469"}, 'remarks':'图片', 'userid':2, 'username':'兔先生', 'createtime':'2021-07-05 09:12:17.0'},
    {'nid':'2', 'keyid':"h1625464240253",'keycode':{"startMeta":{"parentTagName":"DIV","parentIndex":-2,"textOffset":64},"endMeta":{"parentTagName":"DIV","parentIndex":-2,"textOffset":68},"text":"下一个词","id":"tn-1628153598413"}, 'remarks':'下一个词<span>带标签', 'userid':3, 'username':'超超', 'createtime':'2021-07-05 13:51:01.0'}];
```



## 5. 使用方法

```javascript
// 初始化
var annotate = $("#container").touchNote({});
// 获取选区
var data = annotate.getSelection();
// selection对象结果如下，可以获取range对象（注意非js原始的range对象）、选中文本、选中节点等
{
	selection ：js的selection选区对象,
	range = {
			startContainer: 开始节点, 
			startOffset: 开始节点偏移量,
			endContainer: 结束节点, 
			endOffset:结束节点偏移量,
			domRect : 位置信息
	},		
	selectedNodes ：选中的节点,
	text ：选中的文本
}

// 获取选中文本也可以直接使用工具类
touchNote.getSelectionText();
// 移除指定id的高亮，如果对应id的高亮不存在，则返回false,rootEle为调用的jquery对象，callback为回调方法
touchNote.removeHighlight(rootEle, id, callback);
// 移除所有的高亮,rootEle为调用的jquery对象，callback为回调方法
touchNote.removeAllHighlight(rootEle, callback);
```



### 数据提交

一般使用过程中数据都是需要入库保存的，示例中没有进行保存，如果需要保存可以覆盖空实现的submitData方法

```javascript
$("#container").touchNote({
    	// 提交成功返回true，失败返回false
        submitData:function(remarks, keytype, params, ele, dialogEle, options){
            // 此处可以使用ajax自己实现
            console.log("数据提交后执行，如保存数据库等, 正式环境下建议覆盖", remarks, keytype, params);
            return true;
    	}
});
```

如果弹出框不满足要求需要重写，可以参考示例中的codereview.html，重写弹出框以及提交方法。

## 6. 效果图

---
### 简单示例
---
**选中：** 
![fKAanI.png](https://z3.ax1x.com/2021/08/07/fKAOD1.png "简单示例1.png")
**批注：** 
![fKADN8.png](https://z3.ax1x.com/2021/08/07/fKEGV0.png "简单示例2.png")

### 代码审查
---
**效果图：** 
![效果图](https://z3.ax1x.com/2021/08/04/fkQDkF.png "代码审查效果.png")



### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。
| 支付宝                                                       | 微信                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
|  |  |